const limitFunctionCallCount = require("../limitFunctionCallCount");

function multiple(a, b) {
  return a * b;
}

function sayHello(name) {
  return "Hello " + name;
}
let numberMultipleFunction = limitFunctionCallCount(multiple, 2);

console.log(numberMultipleFunction(1, 2));
console.log(numberMultipleFunction(2, 2));
console.log(numberMultipleFunction(2, 3));
console.log(numberMultipleFunction(8, 2));
console.log(numberMultipleFunction(4, 2));

let hello = limitFunctionCallCount(sayHello, 2);

console.log(hello("Akash"));
console.log(hello("Akash"));
console.log(hello("Akash"));
console.log(hello("Akash"));
console.log(hello("Akash"));
