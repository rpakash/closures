const cacheFunction = require("../cacheFunction");

function add(a, b) {
  console.log("Adding ", a, b);

  return a + b;
}

let cacheData = cacheFunction(add);

console.log(cacheData(1, 2));
console.log(cacheData(1, 2));
console.log(cacheData(2, 4));
console.log(cacheData(2, 4));
console.log(cacheData(1, 2));
