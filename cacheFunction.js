function cacheFunction(callback) {
  if (typeof callback !== "function") {
    return;
  }

  let cache = {};

  return function () {
    let key = [...arguments].join("-");

    if (cache.hasOwnProperty(key)) {
      return cache[key];
    } else {
      cache[key] = callback(...arguments);

      return cache[key];
    }
  };
}

module.exports = cacheFunction;
