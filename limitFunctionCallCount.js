function limitFunctionCallCount(callback, nTimes) {
  if (typeof callback !== "function" || !Number.isInteger(nTimes)) {
    return null;
  }

  let invokedTimes = 0;

  return function () {
    if (invokedTimes < nTimes) {
      invokedTimes++;

      let returnedValue = callback(...arguments);

      if (returnedValue === undefined) {
        return null;
      }

      return returnedValue;
    } else {
      return null;
    }
  };
}

module.exports = limitFunctionCallCount;
